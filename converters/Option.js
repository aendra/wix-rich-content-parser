import {
  a as isNone$1,
  l as left$1,
  r as right$1,
  i as identity,
  s as some$1,
  n as none$1,
  u as unsafeUpdateAt$1,
  b as isNonEmpty$1,
  c as isOutOfBound$1,
  p as pipe,
  d as isSome,
  e as isLeft$1,
  f as flow,
} from "./ReadonlyNonEmptyArray.js";
function fromOption$1(F) {
  return function (onNone) {
    return function (ma) {
      return F.fromEither(isNone$1(ma) ? left$1(onNone()) : right$1(ma.value));
    };
  };
}
function fromPredicate$2(F) {
  return function (predicate, onFalse) {
    return function (a) {
      return F.fromEither(predicate(a) ? right$1(a) : left$1(onFalse(a)));
    };
  };
}
var first = function () {
    return { concat: identity };
  },
  concatAll$1 = function (M) {
    return function (startWith) {
      return function (as) {
        return as.reduce(function (a, acc) {
          return M.concat(a, acc);
        }, startWith);
      };
    };
  },
  __spreadArray = function (to, from) {
    for (var i = 0, il = from.length, j = to.length; i < il; i++, j++)
      to[j] = from[i];
    return to;
  },
  separated = function (left, right) {
    return { left: left, right: right };
  };
function wiltDefault(T, C) {
  return function (F) {
    var traverseF = T.traverse(F);
    return function (wa, f) {
      return F.map(traverseF(wa, f), C.separate);
    };
  };
}
function witherDefault(T, C) {
  return function (F) {
    var traverseF = T.traverse(F);
    return function (wa, f) {
      return F.map(traverseF(wa, f), C.compact);
    };
  };
}
var isNonEmpty = isNonEmpty$1,
  isOutOfBound = isOutOfBound$1;
function lookup(i, as) {
  return void 0 === as
    ? function (as) {
        return lookup(i, as);
      }
    : isOutOfBound(i, as)
    ? none$1
    : some$1(as[i]);
}
var findIndex$1 = function (predicate) {
  return function (as) {
    for (var i = 0; i < as.length; i++) if (predicate(as[i])) return some$1(i);
    return none$1;
  };
};
function findFirst(predicate) {
  return function (as) {
    for (var i = 0; i < as.length; i++)
      if (predicate(as[i])) return some$1(as[i]);
    return none$1;
  };
}
var foldMapWithIndex$1 = function (M) {
    return function (f) {
      return function (fa) {
        return fa.reduce(function (b, a, i) {
          return M.concat(b, f(i, a));
        }, M.empty);
      };
    };
  },
  reduceWithIndex$1 = function (b, f) {
    return function (fa) {
      for (var len = fa.length, out = b, i = 0; i < len; i++)
        out = f(i, out, fa[i]);
      return out;
    };
  },
  reduceRightWithIndex$1 = function (b, f) {
    return function (fa) {
      return fa.reduceRight(function (b, a, i) {
        return f(i, a, b);
      }, b);
    };
  },
  unsafeUpdateAt = function (i, a, as) {
    return isNonEmpty(as) ? unsafeUpdateAt$1(i, a, as) : as;
  },
  append = function (end) {
    return function (init) {
      return __spreadArray(__spreadArray([], init), [end]);
    };
  },
  size = function (as) {
    return as.length;
  },
  findIndex = findIndex$1,
  _map$1 = function (fa, f) {
    return pipe(fa, map$2(f));
  },
  _reduce$1 = function (fa, b, f) {
    return pipe(fa, reduce$1(b, f));
  },
  _foldMap$1 = function (M) {
    var foldMapM = foldMap$1(M);
    return function (fa, f) {
      return pipe(fa, foldMapM(f));
    };
  },
  _reduceRight$1 = function (fa, b, f) {
    return pipe(fa, reduceRight$1(b, f));
  },
  _traverse$1 = function (F) {
    var traverseF = traverse$1(F);
    return function (ta, f) {
      return pipe(ta, traverseF(f));
    };
  },
  of$1 = function (a) {
    return [a];
  },
  zero$1 = function () {
    return [];
  },
  map$2 = function (f) {
    return function (fa) {
      return fa.map(function (a) {
        return f(a);
      });
    };
  },
  ap$1 = function (fa) {
    return chain$2(function (f) {
      return pipe(fa, map$2(f));
    });
  },
  chain$2 = function (f) {
    return function (ma) {
      return pipe(
        ma,
        (function (f) {
          return function (as) {
            for (var out = [], i = 0; i < as.length; i++)
              out.push.apply(out, f(i, as[i]));
            return out;
          };
        })(function (_, a) {
          return f(a);
        })
      );
    };
  },
  flatten = chain$2(identity),
  mapWithIndex = function (f) {
    return function (fa) {
      return fa.map(function (a, i) {
        return f(i, a);
      });
    };
  },
  filterMapWithIndex = function (f) {
    return function (fa) {
      for (var out = [], i = 0; i < fa.length; i++) {
        var optionB = f(i, fa[i]);
        isSome(optionB) && out.push(optionB.value);
      }
      return out;
    };
  },
  filterMap$1 = function (f) {
    return filterMapWithIndex(function (_, a) {
      return f(a);
    });
  },
  compact$1 = filterMap$1(identity),
  separate$1 = function (fa) {
    for (var left = [], right = [], _i = 0, fa_1 = fa; _i < fa_1.length; _i++) {
      var e = fa_1[_i];
      "Left" === e._tag ? left.push(e.left) : right.push(e.right);
    }
    return separated(left, right);
  },
  filter$1 = function (predicate) {
    return function (as) {
      return as.filter(predicate);
    };
  },
  partition$1 = function (predicate) {
    return partitionWithIndex(function (_, a) {
      return predicate(a);
    });
  },
  partitionWithIndex = function (predicateWithIndex) {
    return function (as) {
      for (var left = [], right = [], i = 0; i < as.length; i++) {
        var b = as[i];
        predicateWithIndex(i, b) ? right.push(b) : left.push(b);
      }
      return separated(left, right);
    };
  },
  partitionMap$1 = function (f) {
    return partitionMapWithIndex(function (_, a) {
      return f(a);
    });
  },
  partitionMapWithIndex = function (f) {
    return function (fa) {
      for (var left = [], right = [], i = 0; i < fa.length; i++) {
        var e = f(i, fa[i]);
        "Left" === e._tag ? left.push(e.left) : right.push(e.right);
      }
      return separated(left, right);
    };
  },
  alt$1 = function (that) {
    return function (fa) {
      return fa.concat(that());
    };
  },
  filterWithIndex = function (predicateWithIndex) {
    return function (as) {
      return as.filter(function (b, i) {
        return predicateWithIndex(i, b);
      });
    };
  },
  extend$1 = function (f) {
    return function (wa) {
      return wa.map(function (_, i) {
        return f(wa.slice(i));
      });
    };
  },
  foldMap$1 = function (M) {
    var foldMapWithIndexM = foldMapWithIndex$1(M);
    return function (f) {
      return foldMapWithIndexM(function (_, a) {
        return f(a);
      });
    };
  },
  foldMapWithIndex = foldMapWithIndex$1,
  reduce$1 = function (b, f) {
    return reduceWithIndex$1(b, function (_, b, a) {
      return f(b, a);
    });
  },
  reduceWithIndex = reduceWithIndex$1,
  reduceRight$1 = function (b, f) {
    return reduceRightWithIndex$1(b, function (_, a, b) {
      return f(a, b);
    });
  },
  reduceRightWithIndex = reduceRightWithIndex$1,
  traverse$1 = function (F) {
    var traverseWithIndexF = traverseWithIndex(F);
    return function (f) {
      return traverseWithIndexF(function (_, a) {
        return f(a);
      });
    };
  },
  sequence$1 = function (F) {
    return function (ta) {
      return _reduce$1(ta, F.of([]), function (fas, fa) {
        return F.ap(
          F.map(fas, function (as) {
            return function (a) {
              return pipe(as, append(a));
            };
          }),
          fa
        );
      });
    };
  },
  traverseWithIndex = function (F) {
    return function (f) {
      return reduceWithIndex(F.of([]), function (i, fbs, a) {
        return F.ap(
          F.map(fbs, function (bs) {
            return function (b) {
              return pipe(bs, append(b));
            };
          }),
          f(i, a)
        );
      });
    };
  },
  getSemigroup$1 = function () {
    return {
      concat: function (first, second) {
        return first.concat(second);
      },
    };
  },
  getMonoid$1 = function () {
    return {
      concat: function (first, second) {
        return first.concat(second);
      },
      empty: [],
    };
  },
  Compactable$1 = { URI: "Array", compact: compact$1, separate: separate$1 },
  Traversable$1 = {
    URI: "Array",
    map: _map$1,
    reduce: _reduce$1,
    foldMap: _foldMap$1,
    reduceRight: _reduceRight$1,
    traverse: _traverse$1,
    sequence: sequence$1,
  },
  array = {
    URI: "Array",
    compact: compact$1,
    separate: separate$1,
    map: _map$1,
    ap: function (fab, fa) {
      return pipe(fab, ap$1(fa));
    },
    of: of$1,
    chain: function (ma, f) {
      return pipe(ma, chain$2(f));
    },
    filter: function (fa, predicate) {
      return pipe(fa, filter$1(predicate));
    },
    filterMap: function (fa, f) {
      return pipe(fa, filterMap$1(f));
    },
    partition: function (fa, predicate) {
      return pipe(fa, partition$1(predicate));
    },
    partitionMap: function (fa, f) {
      return pipe(fa, partitionMap$1(f));
    },
    mapWithIndex: function (fa, f) {
      return pipe(fa, mapWithIndex(f));
    },
    partitionMapWithIndex: function (fa, f) {
      return pipe(fa, partitionMapWithIndex(f));
    },
    partitionWithIndex: function (fa, predicateWithIndex) {
      return pipe(fa, partitionWithIndex(predicateWithIndex));
    },
    filterMapWithIndex: function (fa, f) {
      return pipe(fa, filterMapWithIndex(f));
    },
    filterWithIndex: function (fa, predicateWithIndex) {
      return pipe(fa, filterWithIndex(predicateWithIndex));
    },
    alt: function (fa, that) {
      return pipe(fa, alt$1(that));
    },
    zero: zero$1,
    unfold: function (b, f) {
      for (var out = [], bb = b; ; ) {
        var mt = f(bb);
        if (!isSome(mt)) break;
        var _a = mt.value,
          a = _a[0],
          b_1 = _a[1];
        out.push(a), (bb = b_1);
      }
      return out;
    },
    reduce: _reduce$1,
    foldMap: _foldMap$1,
    reduceRight: _reduceRight$1,
    traverse: _traverse$1,
    sequence: sequence$1,
    reduceWithIndex: function (fa, b, f) {
      return pipe(fa, reduceWithIndex(b, f));
    },
    foldMapWithIndex: function (M) {
      var foldMapWithIndexM = foldMapWithIndex(M);
      return function (fa, f) {
        return pipe(fa, foldMapWithIndexM(f));
      };
    },
    reduceRightWithIndex: function (fa, b, f) {
      return pipe(fa, reduceRightWithIndex(b, f));
    },
    traverseWithIndex: function (F) {
      var traverseWithIndexF = traverseWithIndex(F);
      return function (ta, f) {
        return pipe(ta, traverseWithIndexF(f));
      };
    },
    extend: function (fa, f) {
      return pipe(fa, extend$1(f));
    },
    wither: witherDefault(Traversable$1, Compactable$1),
    wilt: wiltDefault(Traversable$1, Compactable$1),
  },
  concatAll = function (M) {
    return concatAll$1(M)(M.empty);
  },
  monoidAll = {
    concat: function (x, y) {
      return x && y;
    },
    empty: !0,
  },
  monoidAny = {
    concat: function (x, y) {
      return x || y;
    },
    empty: !1,
  },
  left = left$1,
  right = right$1,
  getSemigroup = function (S) {
    return {
      concat: function (x, y) {
        return isLeft(y)
          ? x
          : isLeft(x)
          ? y
          : right(S.concat(x.right, y.right));
      },
    };
  },
  map$1 = function (f) {
    return function (fa) {
      return isLeft(fa) ? fa : right(f(fa.right));
    };
  },
  chain$1 = function (f) {
    return function (ma) {
      return isLeft(ma) ? ma : f(ma.right);
    };
  },
  FromEither = { URI: "Either", fromEither: identity },
  fromPredicate$1 = fromPredicate$2(FromEither),
  fromOption = fromOption$1(FromEither),
  isLeft = isLeft$1,
  fold$1 = function (onLeft, onRight) {
    return function (ma) {
      return isLeft(ma) ? onLeft(ma.left) : onRight(ma.right);
    };
  },
  tryCatch = function (f, onThrow) {
    try {
      return right(f());
    } catch (e) {
      return left(onThrow(e));
    }
  },
  not = function (predicate) {
    return function (a) {
      return !predicate(a);
    };
  },
  none = none$1,
  some = some$1;
function fromPredicate(predicate) {
  return function (a) {
    return predicate(a) ? some(a) : none;
  };
}
var getRight = function (ma) {
    return "Left" === ma._tag ? none : some(ma.right);
  },
  _map = function (fa, f) {
    return pipe(fa, map(f));
  },
  _reduce = function (fa, b, f) {
    return pipe(fa, reduce(b, f));
  },
  _foldMap = function (M) {
    var foldMapM = foldMap(M);
    return function (fa, f) {
      return pipe(fa, foldMapM(f));
    };
  },
  _reduceRight = function (fa, b, f) {
    return pipe(fa, reduceRight(b, f));
  },
  _traverse = function (F) {
    var traverseF = traverse(F);
    return function (ta, f) {
      return pipe(ta, traverseF(f));
    };
  },
  _filter = function (fa, predicate) {
    return pipe(fa, filter(predicate));
  },
  URI = "Option",
  map = function (f) {
    return function (fa) {
      return isNone(fa) ? none : some(f(fa.value));
    };
  },
  of = some,
  ap = function (fa) {
    return function (fab) {
      return isNone(fab) || isNone(fa) ? none : some(fab.value(fa.value));
    };
  },
  chain = function (f) {
    return function (ma) {
      return isNone(ma) ? none : f(ma.value);
    };
  },
  reduce = function (b, f) {
    return function (fa) {
      return isNone(fa) ? b : f(b, fa.value);
    };
  },
  foldMap = function (M) {
    return function (f) {
      return function (fa) {
        return isNone(fa) ? M.empty : f(fa.value);
      };
    };
  },
  reduceRight = function (b, f) {
    return function (fa) {
      return isNone(fa) ? b : f(fa.value, b);
    };
  },
  alt = function (that) {
    return function (fa) {
      return isNone(fa) ? that() : fa;
    };
  },
  extend = function (f) {
    return function (wa) {
      return isNone(wa) ? none : some(f(wa));
    };
  },
  compact = chain(identity),
  defaultSeparated = separated(none, none),
  separate = function (ma) {
    return isNone(ma)
      ? defaultSeparated
      : separated(
          (function (ma) {
            return "Right" === ma._tag ? none : some(ma.left);
          })(ma.value),
          getRight(ma.value)
        );
  },
  Compactable = { URI: URI, compact: compact, separate: separate },
  filter = function (predicate) {
    return function (fa) {
      return isNone(fa) ? none : predicate(fa.value) ? fa : none;
    };
  },
  filterMap = function (f) {
    return function (fa) {
      return isNone(fa) ? none : f(fa.value);
    };
  },
  partition = function (predicate) {
    return function (fa) {
      return separated(_filter(fa, not(predicate)), _filter(fa, predicate));
    };
  },
  partitionMap = function (f) {
    return flow(map(f), separate);
  },
  traverse = function (F) {
    return function (f) {
      return function (ta) {
        return isNone(ta) ? F.of(none) : F.map(f(ta.value), some);
      };
    };
  },
  sequence = function (F) {
    return function (ta) {
      return isNone(ta) ? F.of(none) : F.map(ta.value, some);
    };
  },
  Traversable = {
    URI: URI,
    map: _map,
    reduce: _reduce,
    foldMap: _foldMap,
    reduceRight: _reduceRight,
    traverse: _traverse,
    sequence: sequence,
  },
  fromEither = getRight,
  isNone = function (fa) {
    return "None" === fa._tag;
  },
  fold = function (onNone, onSome) {
    return function (ma) {
      return isNone(ma) ? onNone() : onSome(ma.value);
    };
  },
  getOrElse = function (onNone) {
    return function (ma) {
      return isNone(ma) ? onNone() : ma.value;
    };
  },
  fromNullable = function (a) {
    return null == a ? none : some(a);
  },
  option = {
    URI: URI,
    map: _map,
    of: of,
    ap: function (fab, fa) {
      return pipe(fab, ap(fa));
    },
    chain: function (ma, f) {
      return pipe(ma, chain(f));
    },
    reduce: _reduce,
    foldMap: _foldMap,
    reduceRight: _reduceRight,
    traverse: _traverse,
    sequence: sequence,
    zero: function () {
      return none;
    },
    alt: function (fa, that) {
      return pipe(fa, alt(that));
    },
    extend: function (wa, f) {
      return pipe(wa, extend(f));
    },
    compact: compact,
    separate: separate,
    filter: _filter,
    filterMap: function (fa, f) {
      return pipe(fa, filterMap(f));
    },
    partition: function (fa, predicate) {
      return pipe(fa, partition(predicate));
    },
    partitionMap: function (fa, f) {
      return pipe(fa, partitionMap(f));
    },
    wither: witherDefault(Traversable, Compactable),
    wilt: wiltDefault(Traversable, Compactable),
    throwError: function () {
      return none;
    },
  },
  getFirstMonoid = function () {
    return (
      (S = first()),
      {
        concat: function (x, y) {
          return isNone(x)
            ? y
            : isNone(y)
            ? x
            : some(S.concat(x.value, y.value));
        },
        empty: none,
      }
    );
    var S;
  };
export {
  isNone as A,
  right as B,
  fromEither as C,
  isLeft as D,
  none as E,
  map as F,
  monoidAny as G,
  monoidAll as H,
  getFirstMonoid as I,
  option as J,
  fromOption as K,
  not as L,
  reduce$1 as M,
  chain$2 as N,
  filter$1 as O,
  flatten as P,
  array as Q,
  findIndex as R,
  getSemigroup$1 as S,
  size as T,
  fold$1 as a,
  fromPredicate$1 as b,
  concatAll as c,
  map$1 as d,
  ap$1 as e,
  first as f,
  fromNullable as g,
  concatAll$1 as h,
  getSemigroup as i,
  chain$1 as j,
  compact$1 as k,
  left as l,
  map$2 as m,
  fromPredicate as n,
  of$1 as o,
  getMonoid$1 as p,
  traverse$1 as q,
  chain as r,
  some as s,
  tryCatch as t,
  fold as u,
  unsafeUpdateAt as v,
  findIndex$1 as w,
  findFirst as x,
  getOrElse as y,
  lookup as z,
};
//# sourceMappingURL=Option-59ba4443.js.map
