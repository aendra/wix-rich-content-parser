import { b as isNonEmpty } from "./ReadonlyNonEmptyArray.js";
var Eq = {
    equals: function (first, second) {
      return first === second;
    },
  },
  Monoid = {
    concat: function (first, second) {
      return first + second;
    },
    empty: "",
  },
  toUpperCase = function (s) {
    return s.toUpperCase();
  },
  replace = function (searchValue, replaceValue) {
    return function (s) {
      return s.replace(searchValue, replaceValue);
    };
  },
  trim = function (s) {
    return s.trim();
  },
  split = function (separator) {
    return function (s) {
      var out = s.split(separator);
      return isNonEmpty(out) ? out : [s];
    };
  },
  includes = function (searchString, position) {
    return function (s) {
      return s.includes(searchString, position);
    };
  };
export {
  Eq as E,
  Monoid as M,
  trim as a,
  includes as i,
  replace as r,
  split as s,
  toUpperCase as t,
};
//# sourceMappingURL=string-80398615.js.map
