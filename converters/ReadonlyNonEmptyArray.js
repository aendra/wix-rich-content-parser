function identity(a) {
  return a;
}
var unsafeCoerce = identity;
function constant(a) {
  return function () {
    return a;
  };
}
var constTrue = constant(!0),
  constFalse = constant(!1);
function flow(ab, bc, cd, de, ef, fg, gh, hi, ij) {
  switch (arguments.length) {
    case 1:
      return ab;
    case 2:
      return function () {
        return bc(ab.apply(this, arguments));
      };
    case 3:
      return function () {
        return cd(bc(ab.apply(this, arguments)));
      };
    case 4:
      return function () {
        return de(cd(bc(ab.apply(this, arguments))));
      };
    case 5:
      return function () {
        return ef(de(cd(bc(ab.apply(this, arguments)))));
      };
    case 6:
      return function () {
        return fg(ef(de(cd(bc(ab.apply(this, arguments))))));
      };
    case 7:
      return function () {
        return gh(fg(ef(de(cd(bc(ab.apply(this, arguments)))))));
      };
    case 8:
      return function () {
        return hi(gh(fg(ef(de(cd(bc(ab.apply(this, arguments))))))));
      };
    case 9:
      return function () {
        return ij(hi(gh(fg(ef(de(cd(bc(ab.apply(this, arguments)))))))));
      };
  }
}
function pipe(a, ab, bc, cd, de, ef, fg, gh, hi) {
  switch (arguments.length) {
    case 1:
      return a;
    case 2:
      return ab(a);
    case 3:
      return bc(ab(a));
    case 4:
      return cd(bc(ab(a)));
    case 5:
      return de(cd(bc(ab(a))));
    case 6:
      return ef(de(cd(bc(ab(a)))));
    case 7:
      return fg(ef(de(cd(bc(ab(a))))));
    case 8:
      return gh(fg(ef(de(cd(bc(ab(a)))))));
    case 9:
      return hi(gh(fg(ef(de(cd(bc(ab(a))))))));
    default:
      for (var ret = arguments[0], i = 1; i < arguments.length; i++)
        ret = arguments[i](ret);
      return ret;
  }
}
var __spreadArray = function (to, from) {
    for (var i = 0, il = from.length, j = to.length; i < il; i++, j++)
      to[j] = from[i];
    return to;
  },
  isNone = function (fa) {
    return "None" === fa._tag;
  },
  isSome = function (fa) {
    return "Some" === fa._tag;
  },
  none = { _tag: "None" },
  some = function (a) {
    return { _tag: "Some", value: a };
  },
  isLeft = function (ma) {
    return "Left" === ma._tag;
  },
  left = function (e) {
    return { _tag: "Left", left: e };
  },
  right = function (a) {
    return { _tag: "Right", right: a };
  },
  has = Object.prototype.hasOwnProperty,
  isNonEmpty = function (as) {
    return as.length > 0;
  },
  isOutOfBound = function (i, as) {
    return i < 0 || i >= as.length;
  },
  unsafeUpdateAt = function (i, a, as) {
    if (as[i] === a) return as;
    var xs = (function (as) {
      return __spreadArray([as[0]], as.slice(1));
    })(as);
    return (xs[i] = a), xs;
  },
  of = function (a) {
    return [a];
  },
  map = function (f) {
    return mapWithIndex(function (_, a) {
      return f(a);
    });
  },
  mapWithIndex = function (f) {
    return function (as) {
      for (var out = [f(0, head(as))], i = 1; i < as.length; i++)
        out.push(f(i, as[i]));
      return out;
    };
  },
  reduce = function (b, f) {
    return reduceWithIndex(b, function (_, b, a) {
      return f(b, a);
    });
  },
  reduceWithIndex = function (b, f) {
    return function (as) {
      return as.reduce(function (b, a, i) {
        return f(i, b, a);
      }, b);
    };
  },
  head = function (as) {
    return as[0];
  };
function filter(predicate) {
  return filterWithIndex(function (_, a) {
    return predicate(a);
  });
}
var filterWithIndex = function (predicate) {
  return function (as) {
    return (function (as) {
      return isNonEmpty(as) ? some(as) : none;
    })(
      as.filter(function (a, i) {
        return predicate(i, a);
      })
    );
  };
};
export {
  isNone as a,
  isNonEmpty as b,
  isOutOfBound as c,
  isSome as d,
  isLeft as e,
  flow as f,
  unsafeCoerce as g,
  has as h,
  identity as i,
  constant as j,
  reduce as k,
  left as l,
  map as m,
  none as n,
  constFalse as o,
  pipe as p,
  of as q,
  right as r,
  some as s,
  filter as t,
  unsafeUpdateAt as u,
  head as v,
  constTrue as w,
};
//# sourceMappingURL=ReadonlyNonEmptyArray-dfc5feb5.js.map
