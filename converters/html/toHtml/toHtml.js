import { f as flow, p as pipe } from "../../ReadonlyNonEmptyArray.js";
import {
  m as map,
  a as fold,
  B as right,
  l as left,
  P as flatten,
  c as concatAll,
  o as of,
  k as compact$1,
  e as ap,
  s as some,
  E as none,
} from "../../Option.js";
import { M as Monoid } from "../../string.js";
import _ from "lodash";
import { f as firstRight } from "../../fp-utils.js";
const { pickBy, identity, compact } = _;
var _DECORATION_TO_HTML_T,
  _NODE_TO_HTML_TAG,
  toHtml = function (content) {
    return "" + nodeArrayToHtml(content.nodes, "\n");
  },
  nodeArrayToHtml = function (nodes, separator) {
    return (
      void 0 === separator && (separator = ""),
      nodes.flatMap(nodeToHtml).join(separator)
    );
  },
  nodeToHtml = function (node) {
    if ("TEXT" === node.type && node.textData)
      return decorateTextElement(node.textData);
    if ("PARAGRAPH" === node.type && 0 === node.nodes.length)
      return EMPTY_PARAGRAPH;
    var tag = NODE_TO_HTML_TAG[node.type];
    "HEADING" === node.type &&
      node.headingData &&
      (tag = "h" + node.headingData.level);
    if (tag) {
      var children = nodeArrayToHtml(node.nodes);
      return createHtmlTag(tag, children);
    }
    return [];
  },
  createLinkAttrs = function (decoration) {
    var _decoration$linkData,
      _decoration$linkData$,
      _decoration$linkData2,
      _decoration$linkData3,
      _decoration$linkData4,
      _decoration$linkData5,
      _decoration$linkData6,
      _decoration$linkData7,
      rel = Object.entries(
        (null === (_decoration$linkData = decoration.linkData) ||
        void 0 === _decoration$linkData ||
        null === (_decoration$linkData$ = _decoration$linkData.link) ||
        void 0 === _decoration$linkData$
          ? void 0
          : _decoration$linkData$.rel) || {}
      )
        .filter(function (_ref) {
          return _ref[0], !!_ref[1];
        })
        .map(function (_ref2) {
          return _ref2[0];
        })
        .join(" "),
      targetRaw =
        null === (_decoration$linkData2 = decoration.linkData) ||
        void 0 === _decoration$linkData2 ||
        null === (_decoration$linkData3 = _decoration$linkData2.link) ||
        void 0 === _decoration$linkData3
          ? void 0
          : _decoration$linkData3.target,
      target = [void 0, "UNRECOGNIZED", "SELF"].includes(targetRaw)
        ? ""
        : "_" + (null == targetRaw ? void 0 : targetRaw.toLowerCase()),
      anchor =
        (null === (_decoration$linkData4 = decoration.linkData) ||
        void 0 === _decoration$linkData4 ||
        null === (_decoration$linkData5 = _decoration$linkData4.link) ||
        void 0 === _decoration$linkData5
          ? void 0
          : _decoration$linkData5.anchor) || "",
      href =
        (null === (_decoration$linkData6 = decoration.linkData) ||
        void 0 === _decoration$linkData6 ||
        null === (_decoration$linkData7 = _decoration$linkData6.link) ||
        void 0 === _decoration$linkData7
          ? void 0
          : _decoration$linkData7.url) || "";
    return pickBy(
      { href: href, rel: rel, anchor: anchor, target: target },
      identity
    );
  },
  createAnchorAttrs = function (decoration) {
    var _decoration$anchorDat;
    return {
      href:
        "#" +
        ((null === (_decoration$anchorDat = decoration.anchorData) ||
        void 0 === _decoration$anchorDat
          ? void 0
          : _decoration$anchorDat.anchor) || ""),
      rel: "noopener noreferrer",
    };
  },
  createMentionAttrs = function (decoration) {
    var _ref3 = decoration.mentionData || {},
      _ref3$name = _ref3.name,
      name = void 0 === _ref3$name ? "" : _ref3$name,
      _ref3$slug = _ref3.slug,
      slug = void 0 === _ref3$slug ? "" : _ref3$slug,
      _ref3$id = _ref3.id,
      id = void 0 === _ref3$id ? "" : _ref3$id;
    return name || slug || id
      ? pickBy(
          {
            role: "link",
            "data-mention-name": name,
            "data-mention-slug": slug,
            "data-mention-id": id,
          },
          identity
        )
      : {};
  },
  createSpoilerAttrs = function () {
    return { "data-spoiler": "true" };
  },
  createColorAttrs = function (decoration) {
    var _ref4 = decoration.colorData || {},
      foreground = _ref4.foreground,
      background = _ref4.background,
      style = pipe(
        [
          foreground && "color: " + foreground + ";",
          background && "background-color: " + background + ";",
        ],
        compact,
        concatAll(Monoid)
      );
    return pickBy({ style: style }, identity);
  },
  isType = function (type) {
    return function (decoration) {
      return decoration.type === type;
    };
  },
  createHtmlTag = function (tag, child, attrs) {
    void 0 === attrs && (attrs = {});
    var isFragment = "fragment" === tag,
      prefix = "li" === tag ? "\n  " : "",
      suffix = ["ul", "ol"].includes(tag) ? "\n" : "";
    return isFragment
      ? "" + child + suffix
      : prefix +
          "<" +
          tag +
          attrsToString(attrs) +
          ">" +
          child +
          suffix +
          "</" +
          tag +
          ">";
  },
  attrsToString = function (attrs) {
    return Object.entries(attrs)
      .map(function (_ref5) {
        return " " + _ref5[0] + '="' + _ref5[1] + '"';
      })
      .join("");
  },
  toForegroundDecoration = function (d) {
    var _d$colorData;
    return null !== (_d$colorData = d.colorData) &&
      void 0 !== _d$colorData &&
      _d$colorData.foreground
      ? some({
          type: "COLOR",
          colorData: { foreground: d.colorData.foreground },
        })
      : none;
  },
  toBackgroundDecoration = function (d) {
    var _d$colorData2;
    return null !== (_d$colorData2 = d.colorData) &&
      void 0 !== _d$colorData2 &&
      _d$colorData2.background
      ? some({
          type: "COLOR",
          colorData: { background: d.colorData.background },
        })
      : none;
  },
  normalizeDecorations = flow(
    map(
      flow(
        function (d) {
          return "COLOR" === d.type ? right(d) : left(d);
        },
        fold(of, function (decoration) {
          return pipe(
            [toBackgroundDecoration, toForegroundDecoration],
            ap(of(decoration)),
            compact$1
          );
        })
      )
    ),
    flatten
  ),
  decorateTextElement = function (_ref6) {
    var text = _ref6.text,
      decorations = _ref6.decorations;
    return normalizeDecorations(decorations).reduce(function (
      child,
      decoration
    ) {
      var tag = DECORATION_TO_HTML_TAG[decoration.type];
      return tag
        ? createHtmlTag(
            tag,
            child,
            (function (decoration) {
              return firstRight(decoration, {}, [
                [isType("LINK"), createLinkAttrs],
                [isType("ANCHOR"), createAnchorAttrs],
                [isType("MENTION"), createMentionAttrs],
                [isType("SPOILER"), createSpoilerAttrs],
                [isType("COLOR"), createColorAttrs],
              ]);
            })(decoration)
          )
        : child;
    },
    text);
  },
  DECORATION_TO_HTML_TAG =
    (((_DECORATION_TO_HTML_T = {}).COLOR = "span"),
    (_DECORATION_TO_HTML_T.MENTION = "span"),
    (_DECORATION_TO_HTML_T.SPOILER = "span"),
    (_DECORATION_TO_HTML_T.BOLD = "strong"),
    (_DECORATION_TO_HTML_T.ITALIC = "em"),
    (_DECORATION_TO_HTML_T.UNDERLINE = "u"),
    (_DECORATION_TO_HTML_T.LINK = "a"),
    (_DECORATION_TO_HTML_T.ANCHOR = "a"),
    _DECORATION_TO_HTML_T),
  NODE_TO_HTML_TAG =
    (((_NODE_TO_HTML_TAG = {}).BULLETED_LIST = "ul"),
    (_NODE_TO_HTML_TAG.COLLAPSIBLE_LIST = "ul"),
    (_NODE_TO_HTML_TAG.ORDERED_LIST = "ol"),
    (_NODE_TO_HTML_TAG.LIST_ITEM = "li"),
    (_NODE_TO_HTML_TAG.COLLAPSIBLE_ITEM = "li"),
    (_NODE_TO_HTML_TAG.COLLAPSIBLE_ITEM_TITLE = "fragment"),
    (_NODE_TO_HTML_TAG.COLLAPSIBLE_ITEM_BODY = "fragment"),
    (_NODE_TO_HTML_TAG.PARAGRAPH = "p"),
    _NODE_TO_HTML_TAG),
  EMPTY_PARAGRAPH = "<p><br /></p>";
export { toHtml };
//# sourceMappingURL=toHtml.js.map
