import{f as flow}from"../../../../ReadonlyNonEmptyArray-dfc5feb5.js";import{r as replace}from"../../../../string-80398615.js";var preprocess=flow(replace(/\s*\n\s*/g,""),replace('"<',"<"),replace('>"',">"),replace(/""/g,'"'),replace(/<\s*br\s*\/?>/g,"\n"));export{preprocess};
//# sourceMappingURL=preprocess.js.map
