import "./ReadonlyNonEmptyArray.js";
import {
  a as fold,
  m as map,
  b as fromPredicate,
  d as map$1,
  c as concatAll,
  e as ap,
  g as fromNullable,
  t as tryCatch,
  h as concatAll$1,
  l as left,
  i as getSemigroup,
  o as of,
  f as first,
} from "./Option.js";
import { p as pipe, i as identity } from "./ReadonlyNonEmptyArray.js";
var MonoidAll = {
    concat: function (first, second) {
      return first && second;
    },
    empty: !0,
  },
  MonoidAny = {
    concat: function (first, second) {
      return first || second;
    },
    empty: !1,
  },
  firstRight = function (candidate, defaultT, resolvers) {
    var firstRightSemi = getSemigroup(first()),
      concatFirstRightSemi = concatAll$1(firstRightSemi)(left(defaultT));
    return pipe(
      concatFirstRightSemi(
        pipe(
          resolvers,
          map(function (r) {
            return pipe(
              candidate,
              fromPredicate(r[0], function () {
                return defaultT;
              }),
              map$1(r[1])
            );
          })
        )
      ),
      fold(identity, identity)
    );
  },
  equals = function (E) {
    return function (lhs) {
      return function (rhs) {
        return E.equals(lhs, rhs);
      };
    };
  },
  concatApply = function (m) {
    return function (fns) {
      return function (data) {
        return pipe(fns, ap(of(data)), concatAll(m));
      };
    };
  },
  and = concatApply(MonoidAll),
  or = concatApply(MonoidAny),
  tap = function (f) {
    return function (data) {
      return f(data), data;
    };
  },
  log = function (tag, processor) {
    return (
      void 0 === processor && (processor = identity),
      tap(
        (function (tag, processor) {
          return (
            void 0 === processor && (processor = identity),
            function (data) {
              return console.log(tag, processor(data));
            }
          );
        })(tag, processor)
      )
    );
  },
  deepLog = function (tag) {
    return tap(
      (function (tag) {
        return function (data) {
          return console.log(tag, JSON.stringify(data, null, 2));
        };
      })(tag)
    );
  },
  getMatches = function (regex) {
    return function (str) {
      return pipe(regex.exec(str), fromNullable);
    };
  },
  stringifyWithReplace = function (replacer, space) {
    return (
      void 0 === space && (space = 2),
      function (a) {
        return tryCatch(function () {
          var s = JSON.stringify(a, replacer, space);
          if ("string" != typeof s)
            throw new Error("Converting unsupported structure to JSON");
          return s;
        }, identity);
      }
    );
  };
export {
  MonoidAny as M,
  and as a,
  concatApply as c,
  deepLog as d,
  equals as e,
  firstRight as f,
  getMatches as g,
  log as l,
  or as o,
  stringifyWithReplace as s,
  tap as t,
};
//# sourceMappingURL=fp-utils-d85d48ef.js.map

//# sourceMappingURL=fp-utils.js.map
