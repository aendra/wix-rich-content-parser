import{uniqueId}from"lodash";function generateId(prefix){return void 0===prefix&&(prefix=""),""+Math.random().toString(36).substr(2,5)+uniqueId(prefix)}export{generateId};
//# sourceMappingURL=generateRandomId.js.map
